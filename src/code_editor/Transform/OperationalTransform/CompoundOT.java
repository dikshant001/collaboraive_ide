package code_editor.Transform.OperationalTransform;

import java.util.ArrayList;

public class CompoundOT {
    OT t;
    ArrayList<OperationDetails> local;
    ArrayList<OperationDetails> received;
    public CompoundOT(ArrayList<OperationDetails> local, ArrayList<OperationDetails> received) {
        t = new OT();
        this.local = local;
        this.received = received;
    }

    public ArrayList<OperationDetails> performCompoundOT() {
        int count1 = 0;
        int count2 = 0;
        ArrayList<OperationDetails> tmp = new ArrayList<>();
        while (count1 < local.size() && count2 < received.size()) {
            tmp.add(0, t.copyDetails(local.get(count1)));
            tmp.add(tmp.size(), t.copyDetails(received.get(count2)));
            ArrayList<OperationDetails> newTmp = new ArrayList<>();
            for (int i = 1; i < tmp.size(); i += 2) {
                t.op1Details = tmp.get(i-1);
                t.op2Details = tmp.get(i);
                t.solve();
                newTmp.add(t.copyDetails(t.transformed1));
                newTmp.add(t.copyDetails(t.transformed2));
            }
            tmp = newTmp;
            count1++;
            count2++;
        }
        while (count1 < local.size()) {
            tmp.add(0, t.copyDetails(local.get(count1)));
            tmp.remove(tmp.size()-1);
            ArrayList<OperationDetails> newTmp = new ArrayList<>();
            for (int i = 1; i < tmp.size(); i += 2) {
                t.op1Details = tmp.get(i-1);
                t.op2Details = tmp.get(i);
                t.solve();
                newTmp.add(t.copyDetails(t.transformed1));
                newTmp.add(t.copyDetails(t.transformed2));
            }
            tmp = newTmp;
            count1++;
        }
        ArrayList<OperationDetails> ans = new ArrayList<>();
        while (count2 < received.size()) {
            ans.add(t.copyDetails(tmp.get(0)));
            tmp.remove(0);
            tmp.add(tmp.size(), t.copyDetails(received.get(count2)));
            ArrayList<OperationDetails> newTmp = new ArrayList<>();
            for (int i = 1; i < tmp.size(); i += 2) {
                t.op1Details = tmp.get(i-1);
                t.op2Details = tmp.get(i);
                t.solve();
                //t.printOperations();
                //t.printTransformedOperations();
                newTmp.add(t.copyDetails(t.transformed1));
                newTmp.add(t.copyDetails(t.transformed2));
            }
            tmp = newTmp;
            count2++;
        }
        while (tmp.size() > 0) {
            ans.add(t.copyDetails(tmp.get(0)));
            //System.out.println(ans.size() + " " + ans.get(ans.size()-1).opName);
            tmp.remove(0);
            tmp.remove(tmp.size()-1);
            ArrayList<OperationDetails> newTmp = new ArrayList<>();
            for (int i = 1; i < tmp.size(); i += 2) {
                t.op1Details = tmp.get(i-1);
                t.op2Details = tmp.get(i);
                t.solve();
                newTmp.add(t.copyDetails(t.transformed1));
                newTmp.add(t.copyDetails(t.transformed2));
            }
            tmp = newTmp;
        }
        return ans;
    }
}
