package code_editor.Transform.OperationalTransform;

import java.util.StringTokenizer;

public class OT {
    private String state;
    private String op1;
    private String op2;
    String newState1;
    String newState2;
    OperationDetails op1Details;
    OperationDetails op2Details;
    OperationDetails transformed1;
    OperationDetails transformed2;
    boolean showState;
    OT() {
        op1Details = new OperationDetails();
        op2Details = new OperationDetails();
        transformed1 = new OperationDetails();
        transformed2 = new OperationDetails();
        showState = false;
    }
    void processOperation(String op, OperationDetails opDetails) {
        StringTokenizer sk = new StringTokenizer(op);
        opDetails.opName = sk.nextToken();
        if (sk.hasMoreTokens()) {
            opDetails.pos = Integer.parseInt(sk.nextToken());
        }
        if (sk.hasMoreTokens()) {
            opDetails.toInsert = sk.nextToken();
        }
    }
    void process() {
        processOperation(getOp1(), op1Details);
        processOperation(getOp2(), op2Details);
    }
    void solve()
    {
        if (op1Details.opName.equals("erase") && op2Details.opName.equals("erase")) {
            if (showState) {
                newState1 = getState().substring(0, op1Details.pos) + getState().substring(op1Details.pos+1);
                newState2 = getState().substring(0, op2Details.pos) + getState().substring(op2Details.pos+1);
            }
            boolean flag = false;
            if (op1Details.pos < op2Details.pos) {
                transformed1.opName = "erase";
                transformed1.pos = op2Details.pos-1;
                transformed2.opName = "erase";
                transformed2.pos = op1Details.pos;
            
            } else if (op1Details.pos == op2Details.pos) {
                transformed1.opName = "erase";
                transformed1.pos = op2Details.pos;
                transformed2.opName = "erase";
                transformed2.pos = op1Details.pos;
                flag = true;
                
            } else {
                transformed1.opName = "erase";
                transformed1.pos = op2Details.pos;
                transformed2.opName = "erase";
                transformed2.pos = op1Details.pos-1;
            }
            transformed1.operationId = op2Details.operationId;
            transformed1.userIdentifier = op2Details.userIdentifier;
            transformed2.operationId = op1Details.operationId;
            transformed2.userIdentifier = op1Details.userIdentifier;
            if (showState && !flag) {
                newState1 = newState1.substring(0, transformed1.pos) + newState1.substring(transformed1.pos+1);
                newState2 = newState2.substring(0, transformed2.pos) + newState2.substring(transformed2.pos+1);
            }
        } else if (op1Details.opName.equals("insert") && op2Details.opName.equals("insert")) {
            if (showState) {
                newState1 = getState().substring(0, op1Details.pos) + op1Details.toInsert + getState().substring(op1Details.pos);
                newState2 = getState().substring(0, op2Details.pos) + op2Details.toInsert + getState().substring(op2Details.pos);
            }
            if (op1Details.pos <= op2Details.pos) {
                transformed1.opName = "insert";
                transformed1.pos = op2Details.pos+1;
                transformed2.opName = "insert";
                transformed2.pos = op1Details.pos;
                transformed1.toInsert = op2Details.toInsert;
                transformed2.toInsert = op1Details.toInsert;
            } else {
                transformed1.opName = "insert";
                transformed1.pos = op2Details.pos;
                transformed2.opName = "insert";
                transformed2.pos = op1Details.pos+1;
                transformed1.toInsert = op2Details.toInsert;
                transformed2.toInsert = op1Details.toInsert;
            }
            transformed1.operationId = op2Details.operationId;
            transformed1.userIdentifier = op2Details.userIdentifier;
            transformed2.operationId = op1Details.operationId;
            transformed2.userIdentifier = op1Details.userIdentifier;
            if (showState) {
                newState1 = newState1.substring(0, transformed1.pos) + transformed1.toInsert + newState1.substring(transformed1.pos);
                newState2 = newState2.substring(0, transformed2.pos) + transformed2.toInsert + newState2.substring(transformed2.pos);
            }
        } else if (op1Details.opName.equals("insert") && op2Details.opName.equals("erase")) {
            if (showState) {
                newState1 = getState().substring(0, op1Details.pos) + op1Details.toInsert + getState().substring(op1Details.pos);
                newState2 = getState().substring(0, op2Details.pos) + getState().substring(op2Details.pos+1);
            }
            if (op1Details.pos < op2Details.pos) {
                transformed1.opName = "erase";
                transformed1.pos = op2Details.pos+1;
                transformed2.opName = "insert";
                transformed2.pos = op1Details.pos;
                transformed2.toInsert = op1Details.toInsert;
            } else if (op1Details.pos == op2Details.pos) {
                transformed1.opName = "erase";
                transformed1.pos = op2Details.pos+1;
                transformed2.opName = "insert";
                transformed2.pos = op1Details.pos;
                transformed2.toInsert = op1Details.toInsert;
            } else {
                transformed1.opName = "erase";
                transformed1.pos = op2Details.pos;
                transformed2.opName = "insert";
                transformed2.pos = op1Details.pos+1;
                transformed2.toInsert = op1Details.toInsert;
            }
            transformed1.operationId = op2Details.operationId;
            transformed1.userIdentifier = op2Details.userIdentifier;
            transformed2.operationId = op1Details.operationId;
            transformed2.userIdentifier = op1Details.userIdentifier;
            if (showState) {
                newState1 = newState1.substring(0, transformed1.pos) + newState1.substring(transformed1.pos+1);
                newState2 = newState2.substring(0, transformed2.pos) + transformed2.toInsert + newState2.substring(transformed2.pos);
            }
        } else if (op1Details.opName.equals("erase") && op2Details.opName.equals("insert")) {
            if (showState) {
                newState1 = getState().substring(0, op1Details.pos) + getState().substring(op1Details.pos+1);
                newState2 = getState().substring(0, op2Details.pos) + op2Details.toInsert + getState().substring(op2Details.pos);
            }
            if (op1Details.pos < op2Details.pos) {
                transformed1.opName = "insert";
                transformed1.pos = op2Details.pos+1;
                transformed1.toInsert = op2Details.toInsert;
                transformed2.opName = "erase";
                transformed2.pos = op1Details.pos;
            } else if (op1Details.pos == op2Details.pos) {
                transformed1.opName = "insert";
                transformed1.pos = op2Details.pos;
                transformed1.toInsert = op2Details.toInsert;
                transformed2.opName = "erase";
                transformed2.pos = op1Details.pos+1;
            } else {
                transformed1.opName = "insert";
                transformed1.pos = op2Details.pos;
                transformed1.toInsert = op2Details.toInsert;
                transformed2.opName = "erase";
                transformed2.pos = op1Details.pos+1;
            }
            transformed1.operationId = op2Details.operationId;
            transformed1.userIdentifier = op2Details.userIdentifier;
            transformed2.operationId = op1Details.operationId;
            transformed2.userIdentifier = op1Details.userIdentifier;
            if (showState) {
                newState1 = newState1.substring(0, transformed1.pos) + transformed1.toInsert + newState1.substring(transformed1.pos);
                newState2 = newState2.substring(0, transformed2.pos) + newState2.substring(transformed2.pos+1);
            }
        }
    }
    void printOperations()
    {
        System.out.println("Operation1 : ");
        System.out.println(op1Details.opName + " " + op1Details.pos + " " +
                ((op1Details.toInsert == null) ? "" : op1Details.toInsert));
        System.out.println("Operation2 : ");
        System.out.println(op2Details.opName + " " + op2Details.pos + " " + 
                ((op2Details.toInsert == null) ? "" : op2Details.toInsert));
    }
    void printTransformedOperations()
    {
        System.out.println("Transformed Operation1 : ");
        System.out.println(transformed1.opName + " " + transformed1.pos + " " +
                ((transformed1.toInsert == null) ? "" : transformed1.toInsert));
        System.out.println("Transformed Operation2 : ");
        System.out.println(transformed2.opName + " " + transformed2.pos + " " + 
                ((transformed2.toInsert == null) ? "" : transformed2.toInsert));
        System.out.println("New State1 : " + newState1);
        System.out.println("New State2 : " + newState2);
    }
    
    String applyOp(String state, OperationDetails op) {
        if (op.opName.equals("insert")) {
            state = state.substring(0, op.pos) + op.toInsert + state.substring(op.pos);
        } else if (op.opName.equals("erase")) {
            state = state.substring(0, op.pos) + state.substring(op.pos+1);
        }
        return state;
    }
    
    String deriveState(String state, OperationDetails op) {
        if (op.opName.equals("insert")) {
            state = state.substring(0, op.pos) + state.substring(op.pos+1);
        } else if (op.opName.equals("erase")) {
            
        }
        return state;
    }
    
    OperationDetails copyDetails(OperationDetails o) {
        OperationDetails op = new OperationDetails();
        op.opName = o.opName;
        op.pos = o.pos;
        op.toInsert = o.toInsert;
        op.operationId = o.operationId;
        op.userIdentifier = o.userIdentifier;
        return op;
    }
    
    public String getState() {
        return state;
    }
    
    public void setState(String state) {
        this.state = state;
    }

    public String getOp1() {
        return op1;
    }

    public void setOp1(String op1) {
        this.op1 = op1;
    }

    public String getOp2() {
        return op2;
    }

    public void setOp2(String op2) {
        this.op2 = op2;
    }
}