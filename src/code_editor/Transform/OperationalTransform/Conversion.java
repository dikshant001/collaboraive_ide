package code_editor.Transform.OperationalTransform;

import code_editor.Operations.EditOperations;
import code_editor.Operations.Operation;
import code_editor.Operations.UserOperations.EraseOperation;
import code_editor.Operations.UserOperations.InsertOperation;
import java.util.ArrayList;

public class Conversion {
	private static OperationDetails toOperationDetails(Operation o) {
		OperationDetails op = new OperationDetails();
		EditOperations type = o.type;
                String typeOp = type.toString();
                switch (typeOp) {
			case "INSERT":
				op.opName = "insert";
				op.pos = ((InsertOperation)o).position;
				String s = "";
                                s += ((InsertOperation)o).charToInsert;
                                op.toInsert = s;
				op.operationId = o.operationId;
				op.userIdentifier = o.userIdentifier;
				break;
			case "ERASE":
				op.opName = "erase";
				op.pos = ((EraseOperation)o).position;
				op.operationId = o.operationId;
				op.userIdentifier = o.userIdentifier;
				break;
		}
                return op;
	}
	private static  Operation toOperation(OperationDetails op) {
		Operation o = null;
		if (op.opName.equals("insert")) {
			o = new InsertOperation(op.operationId, op.userIdentifier, op.pos, op.toInsert.charAt(0));
		} else if (op.opName.equals("erase")) {
			o = new EraseOperation(op.operationId, op.userIdentifier, op.pos);
		}
		return o;
	}	public static ArrayList<Operation> convertToOperation(ArrayList<OperationDetails> op) {
		ArrayList<Operation> o = new ArrayList<>();
		for (int i = 0; i < op.size(); i++) {
			o.add(toOperation(op.get(i)));
		}
		return o;
	}
        
	public static ArrayList<OperationDetails> convertToOperationDetails(ArrayList<Operation> o) {
		ArrayList<OperationDetails> op = new ArrayList<>();
		for (int i = 0; i < o.size(); i++) {
			op.add(toOperationDetails(o.get(i)));
		}
                return op;
	}
}