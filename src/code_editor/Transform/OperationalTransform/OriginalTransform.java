package code_editor.Transform.OperationalTransform;

import java.util.ArrayList;
import code_editor.Operations.Operation;

public class OriginalTransform {
	public static ArrayList<Operation> performTransform(ArrayList<Operation> localOperations, ArrayList<Operation> remoteOperations) {
		ArrayList<OperationDetails> local = Conversion.convertToOperationDetails(localOperations);
		ArrayList<OperationDetails> received = Conversion.convertToOperationDetails(remoteOperations);
		ArrayList<OperationDetails> transformedOpDetails = new ArrayList<>();
		CompoundOT cot = new CompoundOT(local, received);
		transformedOpDetails = cot.performCompoundOT();
		ArrayList<Operation> transformed = new ArrayList<>();
		transformed = Conversion.convertToOperation(transformedOpDetails);
		return transformed;
	}
}