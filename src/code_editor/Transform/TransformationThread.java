package code_editor.Transform;

import code_editor.Operations.Operation;
import code_editor.Transform.OperationalTransform.OriginalTransform;
import code_editor.buffer.BufferInterface;
import java.util.ArrayList;

public class TransformationThread extends Thread{
    String currentUserIdentifier;
    BufferInterface responseBuffer, operationBuffer;
    
    public TransformationThread(String currentUserIdentifier, BufferInterface responseBuffer, BufferInterface operationBuffer) {
        this.currentUserIdentifier = currentUserIdentifier;
        this.responseBuffer = responseBuffer;
        this.operationBuffer = operationBuffer;
    }  
   
    @Override
    public void run(){
        while (true) {
            ArrayList<Operation> operations = (ArrayList<Operation>) responseBuffer.take();
            ArrayList<Operation> transformedOperations = transform(operations);
            if (transformedOperations != null) {
                for (Operation operation: transformedOperations ) {
                    operationBuffer.put(operation);
                }
            }
            clearOperations();
        }
    }
    
    ArrayList<Operation> localOperations = new ArrayList<>();
    private synchronized ArrayList<Operation> transform(ArrayList<Operation> remoteOperations) {
        ArrayList<Operation> transfomedOperations;
        if (localOperations.size() == 0) {
            transfomedOperations = remoteOperations;
        } else {
            transfomedOperations = OriginalTransform.performTransform(localOperations, remoteOperations);
        }
        return transfomedOperations;
    }
    
    public synchronized void addOperation(Operation operation) {
       localOperations.add(operation);
    }
    
    public synchronized void clearOperations() {
        localOperations.clear();
    }
}
