package code_editor.Configurations;

import static code_editor.Configurations.ConfigFilePaths.NetworkConfFilePath;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public abstract class NetworkConfig {
    //Default Values;
    public static String SERVER_URL = "http://127.0.0.1:3000/";
   
    //Registers the user for the first time
    public static String REGISTER_URL = SERVER_URL + "register";
    
    //Has to be initialised during creation of a session
    public static String PUSH_OPERATIONS_URL = SERVER_URL + "push_operation";
    public static String GET_OPERATIONS_URL = SERVER_URL + "get_operation";
    
    public static int POLLING_THREAD_SLEEP_TIME = 500;
    
    public static void getConfig(){
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream(NetworkConfFilePath));
            
            String SERVER_IP = properties.getProperty("SERVER_IP");
            String SERVER_PORT = properties.getProperty("SERVER_PORT");  
            SERVER_URL = "http://" + SERVER_IP + ":" + SERVER_PORT + "/";
            
            POLLING_THREAD_SLEEP_TIME  = Integer.parseInt(properties.getProperty("POLLING_THREAD_SLEEP_TIME"));
            
            setAllConfig();
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException. Error Reading Config File.");
            e.printStackTrace(System.err);
        } catch (IOException e) {
            System.out.println("IO Exception. Error Reading Config File.");
            e.printStackTrace(System.err);
        }
    }
    
    private static void setAllConfig() {
        PUSH_OPERATIONS_URL = SERVER_URL + "push_operation";
        GET_OPERATIONS_URL = SERVER_URL + "get_operation";
    }
}
