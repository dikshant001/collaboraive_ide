package code_editor.EventNotification;

import code_editor.Operations.Operation;

public interface NotificationInterface {
    public void addObserver(ObserverInterface observer);
    public void notifyObservers(Operation operation);
}
