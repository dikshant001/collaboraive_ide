package code_editor.EventNotification;

import code_editor.Operations.Operation;

public interface ObserverInterface {
    void notifyObserver(Operation operation);
}
