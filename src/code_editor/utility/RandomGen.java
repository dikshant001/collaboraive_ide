package code_editor.utility;

import java.math.BigInteger;
import java.security.SecureRandom;

public class RandomGen {

    private static final SecureRandom random = new SecureRandom();

    public static String getRandom() {
        return new BigInteger(130, random).toString(32);
    }
}
