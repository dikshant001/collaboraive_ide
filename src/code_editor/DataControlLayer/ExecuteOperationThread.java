package code_editor.DataControlLayer;

import code_editor.Operations.Operation;
import code_editor.buffer.BufferInterface;

interface ExecuteOperation {
    public void setBuffer(BufferInterface buffer);
    public void doOperations();
}

public class ExecuteOperationThread extends Thread{
    DataControlLayerInterface editorCore;
    BufferInterface operationBuffer;
    
    public ExecuteOperationThread(DataControlLayerInterface editorCore, BufferInterface operationBuffer) {
        this.editorCore = editorCore;
        this.operationBuffer = operationBuffer; 
    }
    
    public void pushOperation(Operation operation) {
        operationBuffer.put(operation);
    }
    
    @Override
    public void run() {
        while (true) {
            try {
                Operation operation = (Operation) operationBuffer.take();
                if (operation != null) {
                    editorCore.performOperation(operation);
                } else {
                    throw new NullPointerException("Operation is null.");
                }
            } catch (NullPointerException e) {
                e.printStackTrace(System.err);
            }
        }
    }
}
