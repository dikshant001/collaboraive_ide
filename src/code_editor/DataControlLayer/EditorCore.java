package code_editor.DataControlLayer;

import code_editor.DataAccessLayer.DataAccessInterface;
import code_editor.DataAccessLayer.Treap;
import code_editor.Operations.Operation;
import code_editor.Operations.UserOperations.EraseOperation;
import code_editor.Operations.UserOperations.InsertOperation;
import code_editor.buffer.Buffer;
import code_editor.buffer.BufferInterface;
import code_editor.EventNotification.NotificationInterface;

public class EditorCore implements DataControlLayerInterface{
    
    BufferInterface operationBuffer = new Buffer();
    
    //----------Exception Class-----------
    class OperationDoesNotExist extends Exception{
        OperationDoesNotExist(String errorMessage) {   
            super(errorMessage);
        }
    }
    //-----------------------------------
    private final DataAccessInterface dataModel;
    
    private final String userIdentifier;
    private final NotificationInterface notificationService; 
    
    public EditorCore(String userIdentifier, NotificationInterface notificationService) {
        this.notificationService = notificationService;
        dataModel = new Treap();
        this.userIdentifier = userIdentifier;   
    }
    
    
    @Override
    public String getUserId() {
        return userIdentifier;
    }
   
    private void insertCharacter(String userIdentifier, int positionToInsert, char character) {
        dataModel.insert(positionToInsert, character);
    }

    private void eraseCharacter(String userIdentifier, int positionToErase) {
        dataModel.erase(positionToErase);
    }
    
    @Override
    public char charAt(int positionToSeek) {
        return dataModel.charAt(positionToSeek);
    }
    
    @Override
    public int getSize() {
        return dataModel.getSize();
    }
    
    @Override
    public void performOperation(Operation operation) {
        try {
            switch(operation.getType().toString()) {
                case "INSERT":{
                    InsertOperation insertOperation = (InsertOperation) operation;
                    this.insertCharacter(insertOperation.userIdentifier, insertOperation.position, insertOperation.charToInsert);   
                } break;
                case "ERASE": {
                    EraseOperation eraseOperation = (EraseOperation) operation;
                    this.eraseCharacter(eraseOperation.userIdentifier, eraseOperation.position);
                } break;
                default: {
                    throw new OperationDoesNotExist("Operation doesnot Exist.");
                }
            }

            if (operation.userIdentifier.equals(getUserId())) {
            } else {
                notificationService.notifyObservers(operation);
            }
        } catch (OperationDoesNotExist e){
            e.printStackTrace(System.err);
        }
    }
}
