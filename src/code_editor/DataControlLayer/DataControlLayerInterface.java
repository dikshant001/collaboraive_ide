package code_editor.DataControlLayer;

import code_editor.Operations.Operation;

public interface DataControlLayerInterface {
    String getUserId();
    void performOperation(Operation operation); 
   
    //Debugging Information
    int getSize();
    char charAt(int positionToErase);
    
}
