package code_editor.buffer;

import code_editor.NetworkLayer.NetworkCallHandlerInterface;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Buffer implements BufferInterface {   
    BlockingQueue buffer = new LinkedBlockingQueue() {};
    NetworkCallHandlerInterface requestHandler;
    
    @Override
    public void setHandler(NetworkCallHandlerInterface requestHandler){
        this.requestHandler = requestHandler;
    }
    
    @Override
    public void put(Object request) {
        try {
            buffer.put(request);
        } catch (InterruptedException ex) {
            ex.printStackTrace(System.err);
        }
    }
    
    @Override
    public Object take() {
        try {
            return buffer.take();
        } catch (InterruptedException ex) {
            ex.printStackTrace(System.err);
        }
        return null;
    }
}