package code_editor.buffer;

import code_editor.NetworkLayer.NetworkCallHandlerInterface;

public interface BufferInterface {
    public void put(Object element);
    public Object take();
    
    public void setHandler(NetworkCallHandlerInterface requestHandler);
}

