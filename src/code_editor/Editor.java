package code_editor;

import java.util.ArrayList;
import project_manager.CollectionFactory;
import project_manager.Collections;
import project_manager.Doc;
import project_manager.Node;
import project_manager.Type;

public class Editor {
    public static void main(String[] args) {
        Collections collection = CollectionFactory.getCollection("swapnil");
        ArrayList<Node> nodeList = collection.getContent();
        for (Node node : nodeList) {
            System.out.println("name:" + node.getName());
            System.out.println("path:" + node.getPath());
            if (node.getType() == Type.COLLECTION) {
                Collections subCollection = (Collections)node;
                subCollection.getContent();
            } else if (node.getType() == Type.DOC) {
                Doc doc = (Doc) node;
                System.out.println(doc.getIdentifier());
            }
        }
        collection.addNode("myFile", Type.DOC);
    }
}
