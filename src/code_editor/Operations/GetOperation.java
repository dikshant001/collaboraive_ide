package code_editor.Operations;

import com.google.gson.Gson;

public class GetOperation {
    EditOperations type;
    String userIdentifier;
    public GetOperation(String userIdentifier) {
        this.type = EditOperations.GET;
        this.userIdentifier = userIdentifier;
    }
    
    public String toJSON(){
        String json = new Gson().toJson(this); 
        return json;
    }
}
