package code_editor.Operations;

public enum EditOperations {
    INSERT("INSERT"),
    ERASE("ERASE"),
    REGISTER("REGISTER"),
    GET("GET");
    
    private final String operation;
    
    private EditOperations(String operation){
        this.operation = operation;
    }
    
    @Override
    public String toString(){
        return this.operation;
    }
}