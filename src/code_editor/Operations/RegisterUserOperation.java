package code_editor.Operations;

public class RegisterUserOperation extends Operation{
    public RegisterUserOperation(String operationId, String userIdentifier) {
        super(operationId, userIdentifier, EditOperations.REGISTER);
    }
}