package code_editor.Operations.UserOperations;

import com.google.gson.Gson;
import code_editor.Operations.EditOperations;
import code_editor.Operations.Operation;

public class InsertOperation extends Operation implements UserOperations {
    public int position;
    public char charToInsert;
    
    public InsertOperation(String json) {
        toJavaObject(json);
    }
    
    public InsertOperation(String operationId, String userIdentifier, int position, char charToInsert){
        super(operationId, userIdentifier, EditOperations.INSERT);
        this.position = position;
        this.charToInsert = charToInsert;       
    }
    
    private void toJavaObject(String json) {
        Gson gson = new Gson();
        InsertOperation jsonEntity = gson.fromJson(json, InsertOperation.class);
        this.charToInsert = jsonEntity.charToInsert;
        this.position = jsonEntity.position;
        this.userIdentifier = jsonEntity.userIdentifier;
        this.operationId = jsonEntity.operationId;
        this.type = EditOperations.INSERT;
    }   
}