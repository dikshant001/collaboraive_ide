package code_editor.Operations.UserOperations;

import com.google.gson.Gson;
import code_editor.Operations.EditOperations;
import code_editor.Operations.Operation;

public class EraseOperation extends Operation implements UserOperations {
    public int position;
    
    public EraseOperation(String json) {
        this.toJavaObject(json);
    }
    
    public EraseOperation(String operationId, String userIdentifier, int position) {
        super(operationId,  userIdentifier, EditOperations.ERASE);
        this.position = position;
    }
    
    private void toJavaObject(String json) {
        Gson gson = new Gson();
        EraseOperation jsonEntity = gson.fromJson(json, EraseOperation.class);
        this.position = jsonEntity.position;
        this.userIdentifier = jsonEntity.userIdentifier;
        this.operationId = jsonEntity.operationId;
        this.type = EditOperations.ERASE;
    }
}
