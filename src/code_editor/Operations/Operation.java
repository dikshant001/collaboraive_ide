package code_editor.Operations;

import com.google.gson.Gson;
public abstract class Operation {
    public String operationId;
    public String userIdentifier;
    public EditOperations type;
    
    public Operation() {   
    }
    
    public Operation(String operationId, String userIdentifier, EditOperations type) {
        this.operationId = operationId;
        this.type = type;
        this.userIdentifier = userIdentifier;
    }
    
    public EditOperations getType() {
        return type;
    }
    
    public String getOperationId() {
        return operationId;
    }
    
    public String toJSON(){
        String json = new Gson().toJson(this); 
        return json;
    }
}
