package code_editor.SessionLayer;

import code_editor.DataControlLayer.DataControlLayerInterface;
import code_editor.DataControlLayer.ExecuteOperationThread;
import code_editor.NetworkLayer.NetworkCallHandlerInterface;
import code_editor.Transform.TransformationThread;
import code_editor.buffer.BufferInterface;
import code_editor.EventNotification.NotificationInterface;
import code_editor.EventNotification.NotificationService;

public abstract class AbstractSessionFactory {
    public abstract BufferInterface createBuffer();
    public abstract NotificationService createNotificationService();
    public abstract NetworkCallHandlerInterface createPollingThread(String userIdentifier, BufferInterface responseBuffer);
    public abstract NetworkCallHandlerInterface createRequestHandlerThread(BufferInterface requestBuffer);
    public abstract DataControlLayerInterface createEditorInstance(String userId, NotificationInterface notificationService);
    public abstract TransformationThread createTranformationThread(String userIdentifier, BufferInterface responseBuffer, BufferInterface operationBuffer);
    public abstract ExecuteOperationThread createExecuteOperationThread(DataControlLayerInterface editorCore, BufferInterface operationBuffer);
}
