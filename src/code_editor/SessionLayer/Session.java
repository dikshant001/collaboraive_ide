package code_editor.SessionLayer;

import code_editor.Configurations.Configuration;
import static code_editor.Configurations.NetworkConfig.GET_OPERATIONS_URL;
import static code_editor.Configurations.NetworkConfig.PUSH_OPERATIONS_URL;
import static code_editor.Configurations.NetworkConfig.REGISTER_URL;
import static code_editor.Configurations.NetworkConfig.SERVER_URL;
import code_editor.DataControlLayer.DataControlLayerInterface;
import code_editor.DataControlLayer.ExecuteOperationThread;
import code_editor.EventNotification.NotificationInterface;
import code_editor.NetworkLayer.NetworkCallHandlerInterface;
import code_editor.NetworkLayer.Request;
import code_editor.Operations.Operation;
import code_editor.Operations.RegisterUserOperation;
import code_editor.Operations.UserOperations.EraseOperation;
import code_editor.Operations.UserOperations.InsertOperation;
import code_editor.Operations.UserOperations.UserOperations;
import code_editor.Transform.TransformationThread;
import code_editor.buffer.BufferInterface;
import code_editor.utility.RandomGen;

public class Session {
    
    private final String docId;
    private final String userId;
    
    public final DataControlLayerInterface editorInstance;
    
    private final ExecuteOperationThread executeOperationThread;
    private final TransformationThread transformationThread;
   
    private final NetworkCallHandlerInterface requestHandlerThread; 
    private final NetworkCallHandlerInterface pollingServiceThread;
    
    public final NotificationInterface notificationService;
    
    private final BufferInterface requestBuffer, responseBuffer, operationBuffer;
            
    public Session(String userId, String docId) {
        Configuration.getConfiguration();
        AbstractSessionFactory sessionFactory = new SessionFactory();
        notificationService = sessionFactory.createNotificationService();
        
        editorInstance = sessionFactory.createEditorInstance(userId, notificationService);
        this.userId = editorInstance.getUserId();
        this.docId = docId;
        
        requestBuffer = sessionFactory.createBuffer();
        responseBuffer = sessionFactory.createBuffer();
        operationBuffer= sessionFactory.createBuffer();
        
        requestHandlerThread = sessionFactory.createRequestHandlerThread(requestBuffer);
        pollingServiceThread = sessionFactory.createPollingThread(userId, responseBuffer); 
        executeOperationThread = sessionFactory.createExecuteOperationThread(editorInstance, operationBuffer);
        
        transformationThread = sessionFactory.createTranformationThread(userId, responseBuffer, operationBuffer);   
    
        //Iniailize the url for appripricate docs
        PUSH_OPERATIONS_URL = PUSH_OPERATIONS_URL + "?docId=" + docId;
        GET_OPERATIONS_URL = GET_OPERATIONS_URL + "?docId=" + docId;
        
        //Register the user on Doc
        registerUserOnDoc();
    }
    
    private void registerUserOnDoc() {
        String registerURL = REGISTER_URL;
        RegisterUserOperation registerOperation = new RegisterUserOperation(RandomGen.getRandom(), userId);
        requestBuffer.put(new Request(registerURL, registerOperation.toJSON()));   
    }
  
    public String startSession() {
        requestHandlerThread.start();
        pollingServiceThread.start();
        executeOperationThread.start();
        transformationThread.start();
        return userId;
    }
    
    public void pushOperation(UserOperations userOperation){
        System.err.println(userOperation.toString());
        if (userOperation instanceof InsertOperation){
            InsertOperation insertOperation = (InsertOperation) userOperation;
            executeOperationThread.pushOperation((Operation) userOperation);
            transformationThread.addOperation(insertOperation);
            requestBuffer.put(new Request(PUSH_OPERATIONS_URL, insertOperation.toJSON()));
        } else if (userOperation instanceof EraseOperation){
            EraseOperation eraseOperation = (EraseOperation) userOperation;
            executeOperationThread.pushOperation((Operation) userOperation);
            transformationThread.addOperation(eraseOperation);
            requestBuffer.put(new Request(PUSH_OPERATIONS_URL, eraseOperation.toJSON()));    
        } 
    }
}