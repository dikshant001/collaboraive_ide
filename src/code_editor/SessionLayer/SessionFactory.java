package code_editor.SessionLayer;

import code_editor.DataControlLayer.DataControlLayerInterface;
import code_editor.DataControlLayer.EditorCore;
import code_editor.DataControlLayer.ExecuteOperationThread;
import code_editor.NetworkLayer.NetworkCallHandlerInterface;
import code_editor.NetworkLayer.PollingServiceThread;
import code_editor.NetworkLayer.RequestHandlerThread;
import code_editor.Transform.TransformationThread;
import code_editor.buffer.Buffer;
import code_editor.buffer.BufferInterface;
import code_editor.EventNotification.NotificationInterface;
import code_editor.EventNotification.NotificationService;

public class SessionFactory extends AbstractSessionFactory{

    @Override
    public NetworkCallHandlerInterface createPollingThread(String userIdentifier, BufferInterface responseBuffer) {
         return new PollingServiceThread(userIdentifier, responseBuffer); 
    }

    @Override
    public NetworkCallHandlerInterface createRequestHandlerThread(BufferInterface requestBuffer) {
        return  new RequestHandlerThread(requestBuffer); 
    }
    @Override
    public DataControlLayerInterface createEditorInstance(String userId, NotificationInterface notificationService) {
        return new EditorCore(userId, notificationService);
    }

    @Override
    public BufferInterface createBuffer() {
        return new Buffer();
    }

    @Override
    public TransformationThread createTranformationThread(String userIdentifier, BufferInterface responseBuffer, BufferInterface operationBuffer) {
        return new TransformationThread(userIdentifier, responseBuffer, operationBuffer);    
    }

    @Override
    public NotificationService createNotificationService() {
        return new NotificationService();
    }

    @Override
    public ExecuteOperationThread createExecuteOperationThread(DataControlLayerInterface editorCore, BufferInterface operationBuffer) {
        return new ExecuteOperationThread(editorCore, operationBuffer);
    }
    
}
