package code_editor.NetworkLayer;

import code_editor.buffer.BufferInterface;
import org.apache.http.HttpResponse;

public interface NetworkCallHandlerInterface {
    void setBuffer(BufferInterface buffer);
    void handleRequest(Request request);
    void handleResponse(HttpResponse response);
    
    void run();
    void start();
}
