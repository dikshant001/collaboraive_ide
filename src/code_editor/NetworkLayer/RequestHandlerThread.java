package code_editor.NetworkLayer;

import code_editor.buffer.BufferInterface;
import java.io.IOException;
import org.apache.http.HttpResponse;

public final class RequestHandlerThread extends Thread implements NetworkCallHandlerInterface{
    private BufferInterface buffer;
    
    public RequestHandlerThread(BufferInterface buffer){    
        setBuffer(buffer);
    }
    
    @Override
    public void setBuffer(BufferInterface buffer){
        this.buffer = buffer;
    }
    
    @Override
    public void handleRequest(Request request) {
        boolean requestSent = false;
        while (!requestSent) {
            try {
                HttpResponse response = SendPostRequest.sendPostRequest(request.getRequestUrl(), request.getSerializedRequest());
                handleResponse(response);
                requestSent = true;
            } catch (IOException e) {
                System.err.println("Unable to send HTTP request. Connection Refused. Retrying...");
            }
        }
    }
   
    @Override
    public void handleResponse(HttpResponse response) {
    }
    
    @Override
    public void run(){
        while (true) {
            Request request = (Request) buffer.take();
            handleRequest(request);
        }
    }
}
