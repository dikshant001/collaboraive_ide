package code_editor.NetworkLayer;

import static code_editor.Configurations.NetworkConfig.GET_OPERATIONS_URL;
import static code_editor.Configurations.NetworkConfig.POLLING_THREAD_SLEEP_TIME;
import code_editor.Operations.Deserializer;
import code_editor.Operations.GetOperation;
import code_editor.Operations.Operation;
import code_editor.buffer.BufferInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

public final class PollingServiceThread extends Thread implements NetworkCallHandlerInterface{
    private final String userIdentifier;
    private BufferInterface buffer;
    
    public PollingServiceThread(String userIdentifier, BufferInterface buffer){    
        this.userIdentifier = userIdentifier;
        setBuffer(buffer);
    }
    
    @Override
    public void setBuffer(BufferInterface buffer){
        this.buffer = buffer;
    }
    
    @Override
    public void handleRequest(Request request) {
        try {
            HttpResponse response = SendPostRequest.sendPostRequest(request.getRequestUrl(), request.getSerializedRequest());    
            handleResponse(response);
        } catch (IOException e) {
            System.err.println("Unable to send HTTP request. Connection Refused. Retrying...");
        }
    }
    
    @Override
    public void handleResponse(HttpResponse response)
    {
        HttpEntity httpEntity = response.getEntity();
        try {
            InputStream inStream = httpEntity.getContent();
            String content = IOUtils.toString(inStream);
            if (content.equals("")) {
            } else {
                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.registerTypeAdapter(Operation.class, new Deserializer());
                Gson gson = gsonBuilder.create();
                
                java.lang.reflect.Type listType = new TypeToken<ArrayList<Operation>>() {}.getType();
                ArrayList<Operation> list = gson.fromJson(content, listType);
                buffer.put(list);
            }
        } catch (IOException | UnsupportedOperationException ex) {
            ex.printStackTrace(System.err);
        } 
    }
    
    @Override
    public void run(){
        GetOperation getRequest = new GetOperation(userIdentifier);
        String serializedRequest = getRequest.toJSON();
        while (true) {
            handleRequest(new Request(GET_OPERATIONS_URL, serializedRequest));
            try {
                Thread.sleep(POLLING_THREAD_SLEEP_TIME);
            } catch (InterruptedException ex) {
            }
        }
    }
}
