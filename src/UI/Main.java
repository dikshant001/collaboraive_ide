package UI;

import java.math.BigInteger;
import java.security.SecureRandom;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

class RandomGenerator {

    private static final SecureRandom random = new SecureRandom();

    public static String getRandom() {
        return new BigInteger(130, random).toString(32);
    }
}

public class Main extends Application {

    
    private Scene scene;

    @Override
    public void start(Stage stage) {
        AceEditor aceEditor = new AceEditor();
        
        System.setProperty("http.proxyHost", "172.31.1.4");
        System.setProperty("http.proxyPort", "8080");
        System.setProperty("https.proxyHost", "172.31.1.4");
        System.setProperty("https.proxyPort", "8080");

        Button btn = new Button("click");

        
        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                aceEditor.insert(5, "Ashish Chauhan");
            }
        });

        // create the scene
        stage.setTitle("CollabEdit");
        AnchorPane vbox = new AnchorPane();
        vbox.getChildren().addAll(aceEditor);
        
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                System.exit(0);
            }
        });

        scene = new Scene(vbox, 750, 500, Color.web("#666970"));
        stage.setScene(scene);
        //scene.getStylesheets().add("webviewsample/BrowserToolbar.css");        
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
