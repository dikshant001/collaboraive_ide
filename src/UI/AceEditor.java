package UI;

import code_editor.utility.RandomGen;
import code_editor.Operations.EditOperations;
import code_editor.Operations.Operation;
import code_editor.Operations.UserOperations.EraseOperation;
import code_editor.Operations.UserOperations.InsertOperation;
import code_editor.SessionLayer.Session;
import code_editor.EventNotification.ObserverInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;

public class AceEditor extends Region implements ObserverInterface {

    Session session;
    String userIdentifier;
    private String text;

    final WebView webView = new WebView();
    final WebEngine webEngine = webView.getEngine();

    public void foo(int pos, String txt) {
        System.out.println(pos + " " + txt);
    }

    public AceEditor() {
        getStyleClass().add("browser");
        String path = System.getProperty("user.dir");
        path = path.replace("\\\\", "/");
        path += "/ace/editor.html";

        webEngine.load("file:///" + path);
        webEngine.getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {
            @Override
            public void changed(ObservableValue ov, Worker.State oldState, Worker.State newState) {
                if (newState == Worker.State.SUCCEEDED) {
                    String userId = RandomGen.getRandom();
                    String docId = RandomGen.getRandom();
                    
                    session = new Session(userId, docId);

                    session.notificationService.addObserver(AceEditor.this);
                    userIdentifier = session.startSession();
                }
            }
        });

        JSObject win = (JSObject) webEngine.executeScript("window");
        win.setMember("jHelper", this);

        webView.addEventHandler(KeyEvent.KEY_PRESSED, keyEvent -> {
            if (keyEvent.isControlDown() && keyEvent.getCode() == KeyCode.V) {
                // PASTE
                final Clipboard clipboard = Clipboard.getSystemClipboard();
                String content = (String) clipboard.getContent(DataFormat.PLAIN_TEXT);

                content = encode(content);

                //System.out.println("before convert2 " + content);
                webEngine.executeScript("pasteContent(\"" + content + "\")");
            }
        });

        // retrieve copy event via javascript:alert
        webEngine.setOnAlert((WebEvent<String> we) -> {
            if (we.getData() != null && we.getData().startsWith("copy: ")) {
                // COPY
                final Clipboard clipboard = Clipboard.getSystemClipboard();
                final ClipboardContent content = new ClipboardContent();
                content.putString(we.getData().substring(6));
                clipboard.setContent(content);
            }
        });
        //String url = Browser.class.getResource("/ace/editor.html").toExternalForm();  
        //webEngine.load(url);
        //add the web view to the scene
        getChildren().add(webView);

    }

    public void insert(int index, String txt) {
        Platform.runLater(() -> {
            webEngine.executeScript("insert(" + index + ",\"" + encode(txt) + "\")");
        });
    }

    public void backSpace(int index) {
        Platform.runLater(() -> {
            webEngine.executeScript("erase(" + (index) + ")");
        });
    }

    private Node createSpacer() {
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        return spacer;
    }

    @Override
    protected void layoutChildren() {
        double w = getWidth();
        double h = getHeight();
        layoutInArea(webView, 0, 0, w, h, 0, HPos.CENTER, VPos.CENTER);
    }

    @Override
    protected double computePrefWidth(double height) {
        return 750;
    }

    @Override
    protected double computePrefHeight(double width) {
        return 500;
    }

    @Override
    public void notifyObserver(Operation operation) {
        if (operation.getType() == EditOperations.INSERT) {
            InsertOperation insertOperation = (InsertOperation) operation;

            Platform.runLater(() -> {
                insert(insertOperation.position, "" + insertOperation.charToInsert);
            });
        } else if (operation.getType() == EditOperations.ERASE) {

            EraseOperation eraseOperation = (EraseOperation) operation;
            Platform.runLater(() -> {
                backSpace(eraseOperation.position);
            });
        }
    }

    public void insertedText(int index, String action, String txt) {
        //System.out.println(index + " " + action + " " + txt);

        if ("remove".equals(action)) {
            EraseOperation eraseOperation = new EraseOperation(RandomGenerator.getRandom(), userIdentifier, index);
            session.pushOperation(eraseOperation);

            return;
        }

        /*GsonBuilder builder = new GsonBuilder();
         Gson gson = builder.create();
        
         Type listType = new TypeToken<ArrayList<String>>() {}.getType();
         ArrayList<String> list = gson.fromJson(txt, listType);*/
        InsertOperation insertOperation = new InsertOperation(RandomGenerator.getRandom(), userIdentifier, index, txt.charAt(0));
        //System.out.println(insertOperation);
        session.pushOperation(insertOperation);

    }

    public void insertedText2(String json, int start, int end) {
        //System.out.println("start end " + start + " " + end);
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        Change change = gson.fromJson(json, Change.class);

        if ("remove".equals(change.action)) {
            int size = 0;
            for (int i = 0; i < change.lines.size(); i++) {
                String line = change.lines.get(i);
                size += line.length();
                if (i + 1 < change.lines.size()) {
                    size++;
                }
            }
            end = start + size;

            for (int i = end - 1; i >= start; i--) {
                EraseOperation eraseOperation = new EraseOperation(RandomGenerator.getRandom(), userIdentifier, i);
                session.pushOperation(eraseOperation);
            }
            return;
        }

        //System.out.println("lines size " + change.lines.size());

        if (change.lines.isEmpty()) {
            return;
        }

        int ind = start;
        for (int i = 0; i < change.lines.size(); i++) {
            for (int j = 0; j < change.lines.get(i).length(); j++) {
                char c = change.lines.get(i).charAt(j);

                InsertOperation insertOperation = new InsertOperation(RandomGenerator.getRandom(), userIdentifier, ind++, c);
                session.pushOperation(insertOperation);
            }

            if (i + 1 < change.lines.size()) {
                InsertOperation insertOperation = new InsertOperation(RandomGenerator.getRandom(), userIdentifier, ind++, '\n');
                session.pushOperation(insertOperation);
            }
        }

    }

    String encode(String content) {
        content = content.replaceAll("[\r\n]", "\n");
        content = content.replaceAll("[\r]", "\n");
        StringBuilder sb = new StringBuilder(content);
        for (int i = 0; i < sb.length(); i++) {
            sb.replace(i, i + 1, "" + ((char) (sb.charAt(i) + 5)));
        }

        TextArea txt;

        return sb.toString();
    }

    public void txt(String txt) {
        text = txt;
    }

    public String getText() {
        webEngine.executeScript("getText()");
        return text;
    }

    public void setText(String txt) {
        webEngine.executeScript("setText(\"" + encode(txt) + "\")");
    }
}

class Change {
    Position start, end;
    String action;

    ArrayList<String> lines;
}

class Position {
    int row, column;
}
