/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import compile.CCompile;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

/**
 *
 * @author Ashish
 */
public class FXMLDocumentController implements Initializable {

    Stage stage;

    @FXML
    private Label runBtn;
    @FXML
    private AnchorPane toolBar;
    @FXML
    private AnchorPane editorContainer;

    AceEditor aceEditor = new AceEditor();
    @FXML
    private Label openBtn;

    @FXML
    private TextArea input;

    @FXML
    private TextArea output;

    @FXML
    private TabPane tabPane;

    @FXML
    private Tab tab2;

    @FXML
    private MenuItem openMenu, closeMenu, runMenu;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void loadEditor() {
        input.setWrapText(true);
        output.setWrapText(true);
        output.setEditable(false);        

        AnchorPane.setLeftAnchor(aceEditor, 0d);
        AnchorPane.setRightAnchor(aceEditor, 0d);
        AnchorPane.setTopAnchor(aceEditor, 0d);
        AnchorPane.setBottomAnchor(aceEditor, 0d);

        editorContainer.getChildren().add(aceEditor);
    }

    @FXML
    public void runClicked() {
        tabPane.getSelectionModel().select(tab2);

        String code = aceEditor.getText();
        String arg = input.getText();
        
        System.out.println("compilation started");
        output.setText("Compilation Started... Please wait!\n");
        
        runBtn.setDisable(true);
        runMenu.setDisable(true);
        Thread thread = new CompileThread(output, runBtn, runMenu, code, arg);
        thread.start();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void openClicked() {
        FileChooser flc = new FileChooser();
        flc.setTitle("Select File");
        ExtensionFilter ext = new ExtensionFilter("C/C++ Files", "*.c", "*.cpp");
        flc.getExtensionFilters().add(ext);

        File tmpFile = flc.showOpenDialog(stage);

        if (tmpFile == null) {
            return;
        }

        try {
            InputStream inp = new FileInputStream(tmpFile);
            byte[] bytes = new byte[(int) tmpFile.length()];
            inp.read(bytes);

            String code = new String(bytes);

            aceEditor.setText(code);
        } catch (Exception ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeClicked() {
        System.exit(0);
    }
}

class CompileThread extends Thread {
    
    TextArea output;
    String code;
    String inp;
    Label lbl;
    MenuItem m1;
    
    public CompileThread(TextArea txt, Label lbl, MenuItem m1, String cd, String in) {
        output = txt;
        code = cd;
        inp = in;
        this.lbl = lbl;
        this.m1 = m1;
    }
    
    @Override
    public void run() {
        File tmpFile = new File("tmpFile.cpp");
        if (tmpFile.exists()) {
            tmpFile.delete();
        }

        try {
            tmpFile.createNewFile();

            PrintWriter out = new PrintWriter(tmpFile);
            out.print(code);
            out.close();

            String path = tmpFile.getAbsolutePath();

            String filename = path;
            CCompile c = new CCompile(filename, inp);

            String outstr = c.doWork();
            System.out.println(outstr);

            Platform.runLater(() -> {
                output.appendText("Output: \n");
                output.appendText(outstr);
            });
            
            File f1 = new File("tmpFile.cpp");
            File f2 = new File("input.txt");
            File f3 = new File("a.exe");
            
            f1.delete();
            f2.delete();
            f3.delete();

            Platform.runLater(() -> {
                lbl.setDisable(false);
                m1.setDisable(false);
            });
        } catch (Exception ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            
            Platform.runLater(() -> {
                lbl.setDisable(false);
                m1.setDisable(false);
            });
        }
    }
}