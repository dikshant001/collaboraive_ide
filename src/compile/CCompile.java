package compile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class CCompile {

    private String filename;
    private String arg;

    public CCompile(String filename, String arg) {
        this.filename = filename;
        this.arg = arg;
    }

    public String doWork() {
        Runtime r = Runtime.getRuntime();
        try {
            r.exec("cmd /c del C:\\Users\\dikshant\\Documents\\NetBeansProjects\\CCompile\\a.exe");
            Thread.sleep(1000);

            BufferedWriter bw = new BufferedWriter(new FileWriter("input.txt"));
            bw.write(arg);
            bw.close();

            Process p = r.exec("cmd /c g++ " + filename);
            StringBuffer sb = new StringBuffer();
            BufferedReader stdErr = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            String line = "";
            while ((line = stdErr.readLine()) != null) {
                sb.append(line).append("\n");
            }

            int val = p.exitValue();

            if (val == 0) {
                System.out.println("Compile Successful");
                //long time1 = System.currentTimeMillis();
                Process p1 = r.exec("cmd /c a.exe < input.txt"); // cmd /c a.exe < input.txt
                //p1.waitFor();
                StringBuffer sb1 = new StringBuffer();
                BufferedReader stdInp = new BufferedReader(new InputStreamReader(p1.getInputStream()));
                String line1 = "";
                while ((line1 = stdInp.readLine()) != null) {
                    sb1.append(line1).append("\n");
                }

                return sb1.toString();
            } else {
                System.out.println("Compile Error");

                return sb.toString();
            }
        } catch (Exception e) {
            System.out.println("Exception e : " + e);
            return "";
        }
    }
    
    public static void main(String[] args) throws IOException, InterruptedException {
        String arg = "2" + "\n" + "1 2\n" + "3 4";
        String filename = "ex.c";
        CCompile c = new CCompile(filename, arg);
        System.out.println(c.doWork());
    }
}

