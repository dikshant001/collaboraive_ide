package project_manager;

import code_editor.NetworkLayer.SendPostRequest;
import code_editor.Operations.Operation;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import static project_manager.Configuration.NetworkConfig.PROJECT_ADD_NODE_URL;
import static project_manager.Configuration.NetworkConfig.PROJECT_VIEW_URL;
import project_manager.NetworkLayer.Request;

public class Collections extends Node {
    private final boolean DO_RETRY = false;
    
    public Collections(String name, String path) {
        super(name, path, Type.COLLECTION);
    }
    
    public void addNode(String name, Type type) {
        Node node = new Node(name, getPath(), type); 
        String nodePath = PROJECT_ADD_NODE_URL + "?path=" + getPath() + "." + getName();
        boolean retry = DO_RETRY;
        do {
            Request request = new Request(nodePath, node.toJson());
            try {
                HttpResponse response = SendPostRequest.sendPostRequest(request.getRequestUrl(), request.getSerializedRequest());        
                retry = true;
            } catch(IOException e) {
                System.err.println("Unable to send HTTP request. Connection Refused. Retrying...");
            }
        } while (!retry);
    }
    
    public ArrayList getContent() {
        boolean retry = DO_RETRY;
        do {
            try {
                ArrayList<Collections> node = new ArrayList<>();
                String projectViewURL = PROJECT_VIEW_URL + "?path=" + getPath() + "." + getName();
                Request request = new Request(projectViewURL, "");
                HttpResponse response = SendPostRequest.sendPostRequest(request.getRequestUrl(), request.getSerializedRequest());
                retry = true;
                HttpEntity httpEntity = response.getEntity();
                try {
                    InputStream inStream = httpEntity.getContent();
                    String serializedContent = IOUtils.toString(inStream);
                    if (serializedContent.equals("")) {
                    } else {
                        System.out.println(serializedContent);
                        
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.registerTypeAdapter(Node.class, new project_manager.utlity.Deserializer());
                        Gson gson = gsonBuilder.create();
                
                        java.lang.reflect.Type listType = new TypeToken<ArrayList<Node>>() {}.getType();
                        ArrayList<Operation> list = gson.fromJson(serializedContent, listType);
                        return list;
                     }
                } catch (IOException | UnsupportedOperationException ex) {
                    ex.printStackTrace(System.err);
                } 
            } catch (IOException e) {
                System.err.println("Unable to send HTTP request. Connection Refused. Retrying...");
            }
        } while (!retry);
        return null;
    }
}
