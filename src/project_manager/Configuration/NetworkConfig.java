package project_manager.Configuration;

import static code_editor.Configurations.ConfigFilePaths.NetworkConfFilePath;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public abstract class NetworkConfig {
    //Default Values;
    public static String SERVER_URL = "http://127.0.0.1:3000/";
   
    //View the project
    public static String PROJECT_VIEW_URL = SERVER_URL + "view";
    public static String PROJECT_ADD_NODE_URL = SERVER_URL + "add_node";
    
    public static void getConfig(){
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream(NetworkConfFilePath));
            
            String SERVER_IP = properties.getProperty("SERVER_IP");
            String SERVER_PORT = properties.getProperty("SERVER_PORT");  
            SERVER_URL = "http://" + SERVER_IP + ":" + SERVER_PORT + "/";
            
            setAllConfig();
        } catch (FileNotFoundException e) {
            System.err.println("FileNotFoundException. Error Reading Config File.");
            e.printStackTrace(System.err);
        } catch (IOException e) {
            System.err.println("IO Exception. Error Reading Config File.");
            e.printStackTrace(System.err);
        }
    }
    
    private static void setAllConfig() {
        PROJECT_VIEW_URL = SERVER_URL + "view";
        PROJECT_VIEW_URL = SERVER_URL + "add_node";
    }
}
